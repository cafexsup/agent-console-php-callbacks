##Agent Console SDK Sample


This is a simple 'roll your own' Live Assist agent console, using the CafeX AssistAgentSDK.

To run this code you will need:

1. A CafeX FCSDK & Live Assist server
2. To point the code references to your own server IP.
3. A PHP web server (e.g. MAMP/WAMP)

